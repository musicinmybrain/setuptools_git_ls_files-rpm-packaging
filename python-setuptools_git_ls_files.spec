%global srcname setuptools_git_ls_files

Name:           python-%{srcname}
Version:        0.1.2
Release:        1%{?dist}
Summary:        Plugin for setuptools that finds all git tracked files

License:        MIT
URL:            https://github.com/anthrotype/%{srcname}/
# If we use the GitHub release tarballs, setuptools_scm complains about missing
# git metadata; we need to use the PyPI releases instead.
Source0:        %{pypi_source}

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildRequires:  pyproject-rpm-macros

%global common_description %{expand:
A plugin for setuptools that finds all git tracked files, including submodules.
The command used is git ls-files --cached --recurse-submodule.}

%description %{common_description}


%package -n python3-%{srcname}
Summary:        %{summary}
Requires:       python3dist(setuptools)
Requires:       git-core

%description -n python3-%{srcname} %{common_description}


%generate_buildrequires
%pyproject_buildrequires


%prep
%autosetup -n %{srcname}-%{version}


%build
%pyproject_wheel


%install
%pyproject_install
%pyproject_save_files %{srcname}


# There are no tests to run.


%files -n python3-%{srcname} -f %{pyproject_files}
%license LICENSE
%doc README.md


%changelog
* Sat Dec 19 2020 Benjamin A. Beasley <code@musicinmybrain.net> - 0.1.1-1
- Initial package
